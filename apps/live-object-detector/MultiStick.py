import sys
graph_folder="/home/pi/ncappzoo/caffe/SSD_MobileNet/"
if sys.version_info.major < 3 or sys.version_info.minor < 4:
    print("Please using python3.4 or greater!")
    exit(1)

if len(sys.argv) > 1:
    graph_folder = sys.argv[1]

from mvnc import mvncapi as mvnc
import numpy as np
import cv2
from os import system
import io, time
from os.path import isfile, join
from queue import Queue
from threading import Thread, Event, Lock
import re
from time import sleep
from time import localtime, strftime
from Visualize import *
from skimage.transform import resize

#from utils import visualize_output
from utils import deserialize_output

# Detection threshold: Minimum confidance to tag as valid detection
CONFIDANCE_THRESHOLD = 0.60 # 60% confidant

write_output = False

# Load the labels file
labels_file = '../../caffe/SSD_MobileNet/labels.txt'
labels = [ line.rstrip('\n') for line in
            open( labels_file ) if line != 'classes\n']

mvnc.SetGlobalOption(mvnc.GlobalOption.LOG_LEVEL, 2)

print ("Try to EnumerateDevices()")
devices = mvnc.EnumerateDevices()
if len(devices) == 0:
    print("No devices found")
    quit()
print(len(devices))

devHandle   = []
graphHandle = []

with open(join(graph_folder, "graph"), mode="rb") as f:
    graph = f.read()

for devnum in range(len(devices)):
    devHandle.append(mvnc.Device(devices[devnum]))

    try:
        devHandle[devnum].DeallocateGraph()
        print ("We succesfully initial DeallocateGraph() on devnum: "+ str(devnum))  
    except:
        print ("We have exception while initial DeallocateGraph() on devnum: "+ str(devnum))  
        pass

    try:
        devHandle[devnum].CloseDevice()
        print ("We succesfully initial CloseDevice() on devnum: "+ str(devnum))  
    except:
        print ("We have exception while initial CloseDevice() on devnum: "+ str(devnum))  
        pass

    print ("Try to OpenDevices()", devnum)
    devHandle[devnum].OpenDevice()

    opt = devHandle[devnum].GetDeviceOption(mvnc.DeviceOption.OPTIMISATION_LIST)
    print (opt)

    graphHandle.append(devHandle[devnum].AllocateGraph(graph))
    graphHandle[devnum].SetGraphOption(mvnc.GraphOption.ITERATIONS, 1)
    iterations = graphHandle[devnum].GetGraphOption(mvnc.GraphOption.ITERATIONS)

    dim = (300,300)

print("\nLoaded Graphs!!!")

camera_device_num = 0
#camera_device_num = "/home/pi/Desktop/dataset/DVD_204119_0.mpg"
#camera_device_num = '/home/pi/Desktop/dataset/31-Jul-2017_20-09-01_2000fr.mp4'
#camera_device_num = '/home/pi/Desktop/dataset/31-Jul-2017_20-48-26_2000fr.mp4'
#camera_device_num = '/home/pi/Desktop/dataset/auto-01.mp4'
while(True):
    cam = cv2.VideoCapture(camera_device_num)
    if cam.isOpened():
        break;
    camera_device_num = camera_device_num + 1
    if camera_device_num > 10:
        break;
        
#cam = cv2.VideoCapture('/home/pi/YoloV2NCS/detectionExample/xxxx.mp4')

if cam.isOpened() != True:
    print("Camera/Movie Open Error!!!")
    quit()

#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 300)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 300)

cam_width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
cam_height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))

print ("cam_width, cam_height ", cam_width, cam_height)

cam_fps = cam.get(cv2.CAP_PROP_FPS)
cam_fourcc = int(cam.get(cv2.CAP_PROP_FOURCC))
print ("cam_fps", cam_fps)
print ("cam_fourcc", cam_fourcc)


if write_output:
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    print ("fourcc", fourcc)


    out = cv2.VideoWriter(filename =
                      '/media/pi/New01/dataset/multistick_ssd_output_'
                      + strftime("%a%d%b%Y_%H%M%S", localtime()) + '.avi',
                      fourcc=cam_fourcc, fps=cam_fps, frameSize=(cam_width, cam_height))

#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 300)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 300)

#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 416)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 234)
#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)

#(WEBカメラのみ)
#捨てるフレームが増えてカクつきが増える代わりに実像とプレディクション枠のズレを軽減
#cam.set(cv2.CAP_PROP_FPS, 100)

lock = Lock()
frameBuffer = []
results = Queue()

def camThread(cam, lock, buff, resQ):
    lastresults = None
    print("press 'q' to quit!\n")
    append = buff.append
    get = resQ.get

    frame_sum = 0
    frame_all_sum = 0
    init_time = time.time()
    pre_time = init_time


    failure = 0
    while failure < 100:
        s, img = cam.read()
        t = time.time()
        if not s:
            failure += 1
            print("Could not get frame")
            continue

        failure = 0
        lock.acquire()
        if len(buff)>10:
            for i in range(10):
                frame_all_sum += 1
                buff.pop()
        append((t,img))
        lock.release()
        results = None

        try:
            results = get(False)
        except:
            pass

        if results == None:
            if lastresults == None:
                pass
            else:
                imdraw = Visualize(img, lastresults, t)
                cv2.imshow('DEMO', imdraw)
                #write video
                if write_output:
                    out.write(imdraw)
        else:
            imdraw = Visualize(img, results, t)
            cv2.imshow('DEMO', imdraw)
            #write video
            if write_output:
                out.write(imdraw)
            lastresults = results

        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
            break

        '''
        # fps calculation
        current_time = time.time()
        frame_sum += 1
        frame_all_sum += 1
        dt = current_time - pre_time
        pre_time = current_time
        fps = frame_sum / (current_time-init_time)
        fps_all = frame_all_sum / (current_time-init_time)
        print (1.0/dt, fps, fps_all)
        #'''

    cv2.destroyAllWindows()

    lock.acquire()
    while len(buff) > 0:
        del buff[0]
    lock.release()

def inferencer(results, lock, frameBuffer, handle, devnum):
    failure = 0
    sleep(1)
    while failure < 100:

        lock.acquire()
        if len(frameBuffer) == 0:
            lock.release()
            failure += 1
            sleep(0.1)
            continue

        (t, imgbuff) = frameBuffer[-1]
        img = imgbuff.copy()
        del frameBuffer[-1]
        failure = 0
        lock.release()

        imgw = img.shape[1]
        imgh = img.shape[0]

        #im,offx,offy = PrepareImage(img, dim)
        im = pre_process_image( img, dim )

        handle.LoadTensor(im.astype(np.float16), 'user object')
        out, userobj = handle.GetResult()

        # Get execution time
        #inference_time = handle.GetGraphOption( mvnc.GraphOption.TIME_TAKEN )

        # Deserialize the output into a python dictionary
        output_dict = deserialize_output.ssd( 
                      out, 
                      CONFIDANCE_THRESHOLD, 
                      img.shape)

        # Print the results (each image/frame may have multiple objects)
        #print( "I found these objects in "
        #        + " ( %.2f ms ):" % ( np.sum( inference_time ) )
        #        + " devnum: " + str(devnum)
        #     )
        
        pyresults = []
        for i in range( 0, output_dict['num_detections'] ):

            # Draw bounding boxes around valid detections 
            (y1, x1) = output_dict.get('detection_boxes_' + str(i))[0]
            (y2, x2) = output_dict.get('detection_boxes_' + str(i))[1]

            bbox = BBox(x1,y1,x2,y2,
                        output_dict.get('detection_scores_' + str(i) ),
                        output_dict.get('detection_classes_' + str(i)),
                        labels[ int(output_dict['detection_classes_' + str(i)]) ],
                        devnum, t
                        )

            pyresults.append(bbox)

            #print( "%3.1f%%\t" % output_dict['detection_scores_' + str(i)] 
            #       + labels[ int(output_dict['detection_classes_' + str(i)]) ]
            #       + ": Top Left: " + str( output_dict['detection_boxes_' + str(i)][0] )
            #       + " Bottom Right: " + str( output_dict['detection_boxes_' + str(i)][1] )
            #       + " devnum: " + str(devnum)
            #     )

        results.put(pyresults)
    print("inferencer for devnum " + str(devnum) + " finished")

def pre_process_image( frame, dim):
    # Resize image [Image size is defined by choosen network, during training]
    img = cv2.resize( frame, tuple( dim ) )

    # Convert RGB to BGR [OpenCV reads image in BGR, some networks may need RGB]
    #if( ARGS.colormode == "rgb" ):
    #    img = img[:, :, ::-1]

    # Mean subtraction & scaling [A common technique used to center the data]
    img = img.astype( numpy.float16 )
    img = ( img - numpy.float16( [127.5, 127.5, 127.5] ) ) * 0.00789
    return img

def PrepareImage(img, dim):
    imgw = img.shape[1]
    imgh = img.shape[0]
    imgb = np.empty((dim[0], dim[1], 3))
    imgb.fill(0.5)

    if imgh/imgw > dim[1]/dim[0]:
        neww = int(imgw * dim[1] / imgh)
        newh = dim[1]
    else:
        newh = int(imgh * dim[0] / imgw)
        neww = dim[0]

    offx = int((dim[0] - neww)/2)
    offy = int((dim[1] - newh)/2)

    imgb[offy:offy+newh,offx:offx+neww,:] = resize(img.copy()/255.0,(newh,neww),1)
    im = imgb[:,:,(2,1,0)]
    return im,offx,offy

def Reshape(out, dim):
    shape = out.shape
    out = np.transpose(out.reshape(wh, int(shape[0]/wh)))  
    out = out.reshape(shape)
    return out

class BBox(object):
    def __init__(self, left, top, right, bottom, confidence, objType, name, devnum, t):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
        self.confidence = confidence
        self.objType = objType
        self.name = name
        self.devnum = devnum
        self.t = t

threads = []

camT = Thread(target=camThread, args=(cam, lock, frameBuffer, results))
camT.start()
threads.append(camT)

for devnum in range(len(devices)):
  t = Thread(target=inferencer, args=(results, lock, frameBuffer, graphHandle[devnum], devnum))
  t.start()
  threads.append(t)

for t in threads:
  t.join()

for devnum in range(len(devices)):
  graphHandle[devnum].DeallocateGraph()
  devHandle[devnum].CloseDevice()

print("\n\nFinished\n\n")